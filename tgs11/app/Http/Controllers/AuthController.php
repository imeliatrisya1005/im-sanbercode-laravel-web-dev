<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg(){
     return view('register');   
    }
    public function welcome(){
     return view('welcome');   
    }
    public function kirim(Request $request)
    {
            $namadepan = $request->input('ndpn');
            $namabelakang = $request->input('nblkg');

            return view('welcome', ["namadepan"=>$namadepan, "namabelakang"=>$namabelakang]);
    }
}
