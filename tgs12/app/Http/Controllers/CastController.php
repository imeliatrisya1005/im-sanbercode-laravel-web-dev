<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  // untukfungsiDB


class CastController extends Controller
{
    public function create(){
        return view ('cast.tambah');
    }

    public function store(Request $request){
             $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        // validasinotnullerror
        ]);  
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]); 
        return redirect('/cast');
    }
    public function index(){        // mengambil semua data yg d insert lalu dilempar 
        $cast =  DB::table('cast')->get();

        return view('cast.index', ['cast' => $cast]);   
    }
    public function show($id){
        $cast_show = DB::table('cast')->find($id);
        return view('cast.bio', ['cast_show'=> $cast_show]);
    }
    public function edit($id){
        $cast_edit = DB::table('cast')->find($id);
        return view('cast.edit', ['cast_edit'=> $cast_edit]);
    }
    public function update($id, Request $request){      
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        DB::table('cast')      // fungsi update
              ->where('id', $id)
              ->update(
                ['nama' => $request->input('nama'),
                    'umur' =>  $request->input('umur'),
                    'bio' =>  $request->input('bio')

                ]  );
                return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast'); 
    }
    
}


