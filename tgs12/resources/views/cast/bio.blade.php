@extends('layout.master')

@section('title')
Detail Bio
@endsection
@section('content')

<h1>{{$cast_show->nama}}, {{$cast_show->umur}} Tahun</h1>
<p>{{$cast_show->bio}}</p>

@endsection