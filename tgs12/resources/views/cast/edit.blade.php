@extends('layout.master')

@section('title')
HaLaman Edit Cast
@endsection
@section('content')

<form action="/cast/{{$cast_edit->id}}" method="post">
            @csrf
            @method('put')      {{-- buat method untuk put, krn tdk bissa dbuat d requestnya --}}
            <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="{{$cast_edit->nama}}" class="form-control" >
            </div>
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
            <div class="form-group">
            <label>Umur</label>
            <input type="text" name="umur" value="{{$cast_edit->umur}}" class="form-control" >
            </div>
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
            <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast_edit->bio}}</textarea>   
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection