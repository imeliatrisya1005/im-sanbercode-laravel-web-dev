@extends('layout.master')

@section('title')
Halaman List Cast
@endsection
@section('content')

    <a href="/cast/create" class="btn btn-primary btn-sm">Tambah Cast</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
   <tr>
    <td>{{$key+1}}</td>
    <td>{{$value->nama}}</td>
    <td>{{$value->umur}}</td>
    <td>
      <form action="/cast/{{$value->id}}" method="POST">
        @csrf
      @method('delete')
      <a class="btn btn-info btn-sm" href="/cast/{{$value->id}}">Bio</a>
      <a class="btn btn-warning btn-sm" href="/cast/{{$value->id}}/edit">Edit</a>
      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
    </form>
    </td>
   </tr>
    @empty
    <tr>
        <td>Tidak ada data</td>
    </tr>
    @endforelse
        </tbody>
      </table>

@endsection
