@extends('layout.master')
@section('tittle')
    Register
@endsection
@section('content')
 <h1>Buat Account Baru!</h1>   
 <h2>Sign Up Form</h2>
<form action="/kirim" method="post">
   @csrf
        <label>First Name :</label><br>
        <input type="text" name="ndpn"> <br><br>
        <label>Last Name :</label> <br>
        <input type="text" name="nblkg"><br><br>
        <label>Gender :</label> <br>
        <input type="radio" value="1" name="gender">Male <br>
        <input type="radio" value="2" name="gender">Female <br>
        <input type="radio" value="3" name="gender">Others <br><br>

        <label>Nationality :</label> <br>
        <select name="negara">
            <option value="1">Indonesia</option>
            <option value="2">Arab</option>
            <option value="3">Germany</option> 
        </select><br><br>

        <label>Language Spoken :</label> <br>
        <input type="checkbox" value="1" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="bahasa">English <br>
        <input type="checkbox" value="3" name="bahasa">Arabic <br>
        <input type="checkbox" value="4" name="bahasa">Others <br><br>

        <label>Bio :</label> <br>
       <textarea cols="20" rows="10"></textarea><br>

       <input type="submit" value="SignUp">
</form>
@endsection