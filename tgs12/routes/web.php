<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'reg']);

Route::get('/welcome',[AuthController::class, 'welcome'] );
Route::post('/kirim',[AuthController::class, 'kirim'] );

Route::get('/data-tables', function(){
    return view('page.data-tables');
});
Route::get('/table', function(){
    return view('page.table');
});

// rute template
Route::get('/master', function(){
    return view('layout.master');
});

// route cast
Route::get('/cast', [CastController::class, 'index']);

Route::get ('/cast/create', [CastController::class, 'create']);
// tambah keDB
Route::post ('/cast', [CastController::class, 'store']);
Route::get ('/cast/{id}', [CastController::class, 'show']);
// update
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// put(for update (kirimdata))
Route::put('/cast/{id}', [CastController::class, 'update']);
// delete
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
